FROM maven:3-openjdk-11-slim as builder
COPY src /src
COPY pom.xml /
RUN mvn -f pom.xml clean package

FROM adoptopenjdk/openjdk11:alpine-jre
COPY --from=builder /target/*.jar app.jar
ENTRYPOINT ["java", "-jar", "/app.jar"]